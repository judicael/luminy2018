
default: all

all: algo-affectation.pdf

see: algo-affectation.pdf
	evince $< &

run:
	make see
	while true; do sleep 2; make all; done

algo-affectation.pdf: jcheader.tex

%.pdf: %.tex
	cd $(dir $@); latexmk -f -pdf ${notdir $<} < /dev/null

%.tex: %.org
	cd $(dir $@); emacs ${notdir $<} --batch -f org-beamer-export-to-latex

.PRECIOUS: %.tex
