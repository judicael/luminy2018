
Ce repository contient une conférence sur la question de l'affectation
dans le supérieur.

Exposé donné à Luminy le 9 mai 2018 : tag conf-luminy-2018.

Exposé donné aux RMLL 2018 : tag conf-rmll-2018.

Le contenu de ce repository est sous licence CC-BY-SA.

J. Courant.
